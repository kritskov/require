package require

// Bool converts the given value to bool.
// If the value's type is bool returns this value.
// Otherwise, returns true if the value is not nil.
func Bool(value any) bool {
	value = dereference(value)
	if value == nil {
		return false
	}

	switch v := value.(type) {
	case bool:
		return v
	case string:
		switch v {
		case "":
			return false
		case "0":
			return false
		case "false":
			return false
		default:
			return true
		}
	default:
		if Byte(value, 1) == 0 {
			return false
		}
		return true
	}
}
