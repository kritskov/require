package require

import (
	"fmt"
)

// String returns the given value as string.
// If the given value is nil returns the given default string or "".
func String(value any, defaultString ...string) string {
	def := ""
	if len(defaultString) > 0 {
		def = defaultString[0]
	}

	value = dereference(value)
	if value == nil {
		return def
	}

	switch v := value.(type) {
	case string:
		return v
	case []byte:
		return string(v)
	case []rune:
		return string(v)
	default:
		return fmt.Sprintf("%v", v)
	}
}
