package require

import "reflect"

func dereference(i any) any {
	if i == nil {
		return nil
	}

	value := reflect.ValueOf(i)
	if value.Kind() == reflect.Ptr {
		if value.IsNil() {
			return nil
		}
		i = reflect.Indirect(value).Interface()
	}

	return i
}
