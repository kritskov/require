package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kritskov/require/pkg/require"
)

func TestRequireStringFromString(t *testing.T) {
	expected := "string"
	actual := require.String(expected)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromNil(t *testing.T) {
	var value *string
	expected := ""

	actual := require.String(value)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromNilWithDefault(t *testing.T) {
	var value *string
	expected := "default"

	actual := require.String(value, expected)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromPointer(t *testing.T) {
	expected := "string"
	pointer := &expected

	actual := require.String(pointer)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromBytes(t *testing.T) {
	expected := "string"
	value := []byte(expected)

	actual := require.String(value)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromRunes(t *testing.T) {
	expected := "string"
	value := []rune(expected)

	actual := require.String(value)

	assert.Equal(t, expected, actual)
}

func TestRequireStringFromNumber(t *testing.T) {
	value := 123
	expected := "123"

	actual := require.String(value)

	assert.Equal(t, expected, actual)
}
