package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kritskov/require/pkg/require"
)

func TestRequireBoolFromNil(t *testing.T) {
	var value *bool = nil
	expected := false

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromPointer(t *testing.T) {
	value := true
	pointer := &value
	expected := true

	actual := require.Bool(pointer)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromBool(t *testing.T) {
	value := true
	expected := true

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromNumber(t *testing.T) {
	value := 1
	expected := true

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromZero(t *testing.T) {
	value := 0
	expected := false

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromString(t *testing.T) {
	value := "test"
	expected := true

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromFalseString(t *testing.T) {
	value := "false"
	expected := false

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}

func TestRequireBoolFromOther(t *testing.T) {
	value := struct{}{}
	expected := true

	actual := require.Bool(value)

	assert.Equal(t, expected, actual)
}
