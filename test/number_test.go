package test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/kritskov/require/pkg/require"
)

func TestRequireIntFromByte(t *testing.T) {
	value := byte(10)
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromInt(t *testing.T) {
	value := 10
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromInt32(t *testing.T) {
	value := int32(10)
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromInt64(t *testing.T) {
	value := int64(10)
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromFloat32(t *testing.T) {
	value := float32(10.5)
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromFloat64(t *testing.T) {
	value := float64(10.5)
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromString(t *testing.T) {
	value := "10"
	expected := 10

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromInvalidString(t *testing.T) {
	value := "invalid"
	expected := 0

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromInvalidStringWithDefault(t *testing.T) {
	value := "invalid"
	expected := 5

	actual := require.Int(value, 5)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromNil(t *testing.T) {
	var value any = nil
	expected := 0

	actual := require.Int(value)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromNilWithDefault(t *testing.T) {
	var value any = nil
	expected := 5

	actual := require.Int(value, 5)

	assert.Equal(t, expected, actual)
}

func TestRequireIntFromPointer(t *testing.T) {
	expected := 10
	pointer := &expected

	actual := require.Int(pointer)

	assert.Equal(t, expected, actual)
}
